const controllers = {}
const User = require('../../model/User')
const Blog = require('../../model/Blog')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
require('dotenv').config()
const secretKey = process.env.SECRET_KEY
const utility = require('../../common/utility')
const sendEmail = require('../../common/send-email')
const { messages, status } = require('../../messages/index')

controllers.register = (req, res) => {
    const { sUserName, sEmail, nMobile, sGender, sPassword } = req.body

    if (!(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/.test(sPassword))) return res.status(status.badRequestStatus).json(messages.passwordValidateMsg)

    bcrypt.hash(sPassword, 10, (error, hashedPassword) => {
        if (error) return res.status(status.notFoundStatus).json({ error: error.message })
        let userData = new User({
            sUserName: sUserName,
            sEmail: sEmail,
            nMobile: nMobile,
            sGender: sGender,
            sPassword: hashedPassword,
            sProfilePhoto: `${req.file.filename}`,
            aToken: []
        })

        userData.save()
            .then(() => {
                return res.status(status.createdStatus).json(messages.registeredSuccessMsg)
            })
            .catch(error => {
                res.status(status.internalServerErrorStatus).json({
                    error: error.message,
                })
                utility.removeExtraPhotos(req.file.filename, "profile_photos")
                return
            })
    })

}

controllers.login = (req, res) => {
    const { sEmail, sUserName, sPassword } = req.body

    User.findOne({ $or: [{ sEmail: sEmail }, { sUserName: sUserName }] })
        .then(user => {
            if (user) {
                bcrypt.compare(sPassword, user.sPassword, (err, result) => {
                    if (err) return res.status(status.notFoundStatus).json({ error: err.message })
                    if (result) {
                        let token = jwt.sign(
                            {
                                id: user._id
                            },
                            secretKey, { expiresIn: '5d' })
                        if (user.aToken.length > 0) return res.status(status.badRequestStatus).json({ message: messages.alreadyLoginMsg, token: user.aToken[0] })
                        user.aToken.push(token)
                        user.save().then().catch(console.log)
                        return res.status(status.successStatus).json({
                            message: messages.loginSuccessMsg,
                            token
                        })
                    }
                    else {
                        return res.status(status.badRequestStatus).json(messages.incorrectPasswordMsg)
                    }
                })
            }
            else {
                return res.status(status.notFoundStatus).json(messages.userNotFound)
            }
        })
}

controllers.editProfile = async (req, res) => {
    try {
        if (req.body.sPassword) return res.status(status.badRequestStatus).json(messages.notEditPasswordMsg)
        const currentUser = jwt.verify(req.token, secretKey)
        const user = await User.findOne({ _id: currentUser.id })
        Object.assign(user, req.body)

        if (req.file) {
            utility.removeExtraPhotos(user.profilePhoto, 'profile_photos')
            user.profilePhoto = req.file.filename
        }

        await user.save()
        return res.status(status.successStatus).json(messages.dataUpdateSuccessMsg)
    } catch (error) {
        return res.status(status.internalServerErrorStatus).json({ error: error.message })
    }
}

controllers.changePassword = async (req, res) => {
    try {
        const { sPassword } = req.body
        const currentUser = jwt.verify(req.token, secretKey)

        if (!(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/.test(sPassword))) return res.status(status.badRequestStatus).json(messages.passwordValidateMsg)

        bcrypt.hash(sPassword, 10, async (err, hashedPassword) => {
            if (err) return res.status(status.internalServerErrorStatus).json({ error: err.message })

            const user = await User.findOne({ _id: currentUser.id })

            user.sPassword = hashedPassword
            await user.save()
        })
        return res.status(status.successStatus).json(messages.passwordChangedSuccessMsg)
    } catch (error) {
        return res.status(status.internalServerErrorStatus).json({ error: error.message })
    }
}

controllers.logout = async (req, res) => {
    try {
        const currentUser = jwt.verify(req.token, secretKey)
        const user = await User.findOne({ _id: currentUser.id })
        const keepTokens = user.aToken.filter(e => e !== req.token)
        user.aToken = keepTokens
        await user.save()
        return res.status(status.successStatus).json(messages.loggedOutSuccessMsg)
    } catch (error) {
        return res.status(status.internalServerErrorStatus).json({ error: error.message })
    }
}

controllers.forgotPassword = async (req, res) => {
    try {
        const { sEmail } = req.body
        const user = await User.findOne({ sEmail: sEmail })
        if (!user) return res.status(status.badRequestStatus).json({ message: 'Email not registered' })

        const secret = secretKey + user.userName

        const token = jwt.sign({ id: user._id }, secret, { expiresIn: '20m' })
        const link = `click on this link to reset your password : http://localhost:${process.env.PORT}/user/reset-password/${user._id}/${token}`

        sendEmail(req, res, sEmail, link)
    } catch (error) {
        return res.status(status.internalServerErrorStatus).json({ error: error.message })
    }
}

controllers.resetPassword = async (req, res) => {
    try {
        const { iId, aToken } = req.params
        const { sPassword } = req.body

        if (!(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/.test(sPassword))) return res.status(status.badRequestStatus).json(messages.passwordValidateMsg)

        const user = await User.findOne({ _id: iId })
        if (!user) return res.json(messages.invalidIdMsg)

        const secret = secretKey + user.userName

        jwt.verify(aToken, secret)

        bcrypt.hash(sPassword, 10, async (err, hashedPassword) => {
            if (err) return res.status(status.internalServerErrorStatus).json({ error: err.message })

            user.sPassword = hashedPassword
            await user.save()
            return res.status(status.successStatus).json(messages.passwordResetSuccessMsg)
        })
    } catch (error) {
        return res.status(status.internalServerErrorStatus).json({ error: error.message })
    }
}

controllers.postTheBlog = async (req, res) => {
    try {
        const currentUser = jwt.verify(req.token, secretKey)
        const { sTitle, sDescription, dPublishDate } = req.body
        const cDate = Date.now()
        var pDate = new Date(dPublishDate).getTime()
        const userName = await User.findOne({ _id: currentUser.id })
        var isPublished = false

        if (!(pDate > cDate)) {
            if (new Date(dPublishDate).getDate() === new Date().getDate()) {
                pDate = cDate
                isPublished = true
            } else {
                res.status(status.badRequestStatus).json(messages.invalidDateMsg)
                utility.removeExtraPhotos(req.file.filename, "cover_photos")
                return
            }
        }
        let blog = new Blog({
            sUserName: userName.sUserName,
            sTitle: sTitle,
            sDescription: sDescription,
            sCoverPhoto: `${req.file.filename}`,
            dCreateDate: cDate,
            dPublishDate: pDate,
            bIsPublished: isPublished
        })

        await blog.save()
        const time = pDate - cDate
        utility.eventEmitter(time, sTitle)
        return res.status(status.successStatus).json(messages.blogPostedSuccessMsg)

    } catch (error) {
        res.status(status.internalServerErrorStatus).json({ error: error.message })
        utility.removeExtraPhotos(req.file.filename, "cover_photos")
        return
    }
}

controllers.listOfBlogs = async (req, res) => {
    try {
        const blogs = await Blog.find()
        const publishedBlogs = blogs.filter(e => e.dPublishDate <= Date.now())
        return res.status(status.successStatus).json({ Blogs: publishedBlogs })
    } catch (error) {
        return res.status(status.internalServerErrorStatus).json({ error: error.message })
    }
}

controllers.deleteProfile = async (req, res) => {
    try {
        const currentUser = jwt.verify(req.token, secretKey)
        const user = await User.findOne({ _id: currentUser.id })
        utility.removeExtraPhotos(user.profilePhoto, 'profile_photos')

        const blog = await Blog.find({ user: currentUser.id })
        const blogCount = await Blog.find({ user: currentUser.id }).count()

        if (blogCount > 0) {
            await Blog.deleteMany({ user: currentUser.id })
            blog.map(e => {
                utility.removeExtraPhotos(e.coverPhoto, 'cover_photos')
            })
        }
        await User.deleteOne({ _id: currentUser.id })
        return res.status(status.nocontentStatus).json(messages.profileDelSuccessMsg)
    } catch (error) {
        return res.status(status.internalServerErrorStatus).json({ error: error.message })
    }
}

controllers.likeTheBlog = async (req, res) => {
    try {
        const currentUser = jwt.verify(req.token, secretKey)
        const blogId = req.params.id
        const blog = await Blog.findOne({ _id: blogId })
        if (blog.publishDate > Date.now()) return res.status(status.badRequestStatus).json(messages.blogNotPublishedMsg)

        if (blog.likedBy.findIndex(e => e === currentUser.id) > -1) {
            blog.likes--
            const keepUser = blog.likedBy.filter(e => e !== currentUser.id)
            blog.likedBy = keepUser
            await blog.save()
            return res.status(status.successStatus).json(messages.unlikeSuccessMsg)
        } else {
            blog.likes++
            blog.likedBy.push(currentUser.id)
            await blog.save()
            return res.status(status.successStatus).json(messages.likeSuccessMsg)
        }
    } catch (error) {
        return res.status(status.internalServerErrorStatus).json({ error: error.message })
    }
}

module.exports = controllers