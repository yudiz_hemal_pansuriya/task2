const controllers = require('./controllers')
const utility = require('../../common/utility')
const middleware = require('../../common/middleware')
const app = require('express').Router()

const uploadPP = utility.uploadProfilePhoto()

app.post('/register', uploadPP.single('sProfilePhoto'), controllers.register)
app.post('/login', controllers.login)
app.patch('/edit-profile', uploadPP.single('sProfilePhoto'), middleware.verifyToken, controllers.editProfile)
app.patch('/change-password', middleware.verifyToken, controllers.changePassword)
app.post('/forgot-password', controllers.forgotPassword)
app.post('/reset-password/:iId/:aToken', controllers.resetPassword)
app.get('/logout', middleware.verifyToken, controllers.logout)

const uploadCP = utility.uploadCoverPhoto()

app.post('/post-the-blog', uploadCP.single('sCoverPhoto'), middleware.verifyToken, controllers.postTheBlog)
app.get('/list-of-blogs', middleware.verifyToken, controllers.listOfBlogs)
app.delete('/delete-profile', middleware.verifyToken, controllers.deleteProfile)
app.get('/like-button/:id', middleware.verifyToken, controllers.likeTheBlog)

module.exports = app