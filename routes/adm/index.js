const controllers = require('./controllers')
const app = require('express').Router()
const utility = require('../../common/utility')

const uploadPP = utility.uploadProfilePhoto()

app.post('/dashboard', controllers.dashboard)
app.post('/create-user', uploadPP.single('sProfilePhoto'), controllers.createUser)
app.get('/delete-user/:id', controllers.deleteUser)
app.get('/list-of-user', controllers.listOfUsers)
app.get('/list-of-blogs', controllers.listOfBlogs)
app.post('/blogs-by-date', controllers.blogsByDate)

module.exports = app
// search = userName, title, description
// filter = publishdate range, createddate range, published/created, sort(inc/dec)