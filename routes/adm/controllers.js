const controllers = {}
const Blog = require('../../model/Blog')
const User = require('../../model/User')
const bcrypt = require('bcryptjs')
const utility = require('../../common/utility')
const { messages, status } = require('../../messages/index')

controllers.dashboard = async (req, res) => {
    try {
        let { nPage } = req.body
        if (!nPage) {
            nPage = 0
        }
        const blogs = await Blog.find().sort({ likes: -1 }).skip(nPage * 2).limit(2)
        return res.status(status.successStatus).json({ Blogs: blogs })
    } catch (error) {
        return res.status(status.internalServerErrorStatus).json({ error: error.message })
    }
}

controllers.createUser = async (req, res) => {
    try {
        const { sRole, sUserName, sEmail, nMobile, sGender, sPassword } = req.body

        if (!(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/.test(sPassword))) return res.status(status.badRequestStatus).json(messages.passwordValidateMsg)

        bcrypt.hash(sPassword, 10, async (error, hashedPassword) => {
            if (error) return res.json({ error: error.message })

            let userData = new User({
                sRole: sRole,
                sUserName: sUserName,
                sEmail: sEmail,
                nMobile: nMobile,
                sGender: sGender,
                sPassword: hashedPassword,
                sProfilePhoto: `${req.file.filename}`,
                aToken: []
            })

            const user = await userData.save()
            return res.status(status.createdStatus).json({ message: messages.userCreatedMsg, user: user })
        })
    } catch (error) {
        res.status(status.internalServerErrorStatus).json({ error: error.message })
        utility.removeExtraPhotos(req.file.filename, "profile_photos")
        return
    }
}

controllers.deleteUser = async (req, res) => {
    try {
        const uid = req.params.id
        const user = await User.findOne({ _id: uid })
        utility.removeExtraPhotos(user.profilePhoto, 'profile_photos')

        const blog = await Blog.find({ user: uid })
        const blogCount = await Blog.find({ user: uid }).count()

        if (blogCount > 0) {
            await Blog.deleteMany({ user: uid })
            blog.map(e => {
                utility.removeExtraPhotos(e.coverPhoto, 'cover_photos')
            })
        }
        await User.deleteOne({ _id: uid })
        return res.status(status.nocontentStatus).json(messages.profileDelSuccessMsg)
    } catch (error) {
        return res.status(status.internalServerErrorStatus).json({ error: error.message })
    }
}

controllers.listOfUsers = async (req, res) => {
    try {
        const users = await User.find()
        return res.status(status.successStatus).json({ Users: users })
    } catch (error) {
        return res.status(status.internalServerErrorStatus).json({ error: error.message })
    }
}

controllers.listOfBlogs = async (req, res) => {
    try {

        const { search, sort } = req.body 
        const { pFrom, pTo } = req.body.filterPublished 
        const { cFrom, cTo } = req.body.filterCreated 
        const { published } = req.body
        
        const blogs = await Blog.aggregate([
            {$facet: 
                {
                    "search": [
                        {$match: {$or: [{sUserName: search}, {sTitle: search}, {sDescription: search}]}}
                    ],
                    "filterpublished": [{$match: {dPublishDate: {$gte: utility.getTime(pFrom), $lte: utility.getTime(pTo)}}}],
                    "filterCreated": [{$match: {dCreateDate: {$gte: utility.getTime(cFrom), $lte: utility.getTime(cTo)}}}],
                    "published": [{$match: {bIsPublished: published}}],
                    "sort": [{$sort: sort}]
                }
            }
        ])
        
        return res.status(status.successStatus).json({ Blogs: blogs })
    } catch (error) {
        return res.status(status.internalServerErrorStatus).json({ error: error.message })
    }
}

controllers.blogsByDate = async (req, res) => {
    try {
        const { dFrom, dTo } = req.body
        const start = new Date(dFrom).getTime()
        const end = new Date(dTo).getTime()
        const blogsByDate = await Blog.find({ publishDate: {$gt: start, $lt: end} })
        return res.status(status.successStatus).json({ blogs: blogsByDate })
    } catch (error) {
        return res.status(status.internalServerErrorStatus).json({ error: error.message })
    }
}

module.exports = controllers