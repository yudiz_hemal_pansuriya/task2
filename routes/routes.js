const app = require('express').Router()
const admin = require('./adm/index')
const user = require('./user/index')
const middleware = require('../common/middleware')
const {messages, status} = require('../messages/index')

app.use('/admin', middleware.verifyToken, middleware.checkAdmin, admin)
app.use('/user', user)

app.all('*', (req, res) => {
    return res.status(status.notFoundStatus).json(messages.routeNotFound)
})

module.exports = app