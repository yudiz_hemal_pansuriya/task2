const mongoose = require('mongoose')
require('../db/index')

const userSchema = new mongoose.Schema({
    sRole: {
        type: String,
        default: 'user'
    },
    sUserName: {
        type: String,
        required: [true, "userName is required"],
        unique: true,
        maxlength: 15
    },
    sEmail: {
        type: String,
        required: [true, "email is required"],
        unique: true,
        validate: {
            validator: (email) => {
                return (/^[a-z0-9_.]+@[a-z]+\.[a-z]{2,3}/.test(email))
            },
            message: email => `${email.value} is not valid, Enter valid email`
        }
    },
    nMobile: {
        type: Number,
        required: [true, "mobile is required"],
        validate: {
            validator: (mobile) => {
                return (/^\d{10}$/.test(mobile))
            },
            message: mobile => `${mobile.value} is not valid, Enter valid Mobile`
        }
    },
    sGender: {
        type: String,
        required: [true, "gender is required"],
        validate: {
            validator: (gender) => {
                return (gender === "male" || gender === "female")
            },
            message: gender => `${gender.value} shoulb be either 'male' or 'female'`
        }
    },
    sPassword: {
        type: String,
        required: [true, "password is required"]
    },
    sProfilePhoto: {
        type: String,
        required: [true, "profilePhoto file is required"]
    },
    aToken: {
        type: Array
    }
})

const User = new mongoose.model('User', userSchema)

module.exports = User