const mongoose = require('mongoose')
require('../db/index')

const blogSchema = new mongoose.Schema({
    sUserName: {
        type: String
    },
    sTitle: {
        type: String,
        required: [true, "title is required"],
        unique: true,
        maxlength: 25
    },
    sDescription: {
        type: String,
        required: [true, "description is required"],
        maxlength: 100
    },
    dCreateDate: {
        type: Date
    },
    dPublishDate: {
        type: Date,
        default: Date.now()
    },
    sCoverPhoto: {
        type: String,
        required: [true, "coverImage is required"],
    },
    nLikes: {
        type: Number,
        default: 0
    },
    aLikedBy: {
        type: Array,
        default: []
    },
    bIsPublished: {
        type: Boolean
    }
})

const Blog = new mongoose.model('Blog', blogSchema)

module.exports = Blog