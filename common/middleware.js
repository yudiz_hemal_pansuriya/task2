const middleware = {}
require('dotenv').config()
const secretKey = process.env.SECRET_KEY
const jwt = require('jsonwebtoken')
const User = require('../model/User')
const { messages, status } = require('../messages/index')

middleware.verifyToken = (req, res, next) => {
    const bearerHeader = req.headers['authorization']
    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ')
        const bearerToken = bearer[1]
      
        try {
            const authData = jwt.verify(bearerToken, secretKey)
            User.findOne({ _id: authData.id })
                .then(user => {
                    if(!user) return res.status(status.notFoundStatus).json(messages.userNotFound)
                    const userToken = user.aToken
                    if (userToken.findIndex(e => e === bearerToken) < 0)  return res.status(status.badRequestStatus).json(messages.needtToLoginFirstMsg)
                })
        } catch (error) {
            if (error.message === "jwt expired") {
                const authData = jwt.verify(bearerToken, secretKey, { ignoreExpiration: true })
                User.findOne({ _id: authData.id })
                .then(user => {
                    const userToken = user.aToken
                    const keepTokens = userToken.filter(e => e !== bearerToken)
                    user.aToken = keepTokens
                    user.save().then().catch(console.log)
                })
                
                return res.status(status.badRequestStatus).json({ error: error.message, message: messages.needToLoginAgainMsg })
            }
            else {
                return res.status(status.internalServerErrorStatus).json({ error: error.message })
            }
        }
        req.token = bearerToken
        next()
    } else {
        return res.status(status.unauthorizedStatus).json(messages.headerMissingMsg)
    }
}

middleware.checkAdmin = async (req, res, next) => {
    try{
        const currentUser = jwt.verify(req.token, secretKey)
        const user = await User.findOne({ _id: currentUser.id })
        if(user.sRole !== 'admin') return res.status(status.forbiddenStatus).json(messages.onlyAdminCanMsg)
        next()
    } catch(error) {
        return res.status(status.internalServerErrorStatus).json({ error: error.message })
    }
}

module.exports = middleware