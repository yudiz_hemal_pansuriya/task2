const nodemailer = require('nodemailer')
require('dotenv').config()
const { status } = require('../messages/index')

const sendEmail = (req, res, email, link) => {
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'hemal.pansuriya@yudiz.com',
            pass: process.env.EMAIL_PASSWORD
        }
    })

    const mailOptions = {
        from: 'hemal.pansuriya@yudiz.com',
        to: email,
        subject: 'Password link',
        text: link
    }

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            return res.status(status.internalServerErrorStatus).json({ error: error.message })
        } else {
            return res.status(status.successStatus).json({message: 'Email sent: ' + info.response})
        }
    })
}

module.exports = sendEmail