const multer = require("multer")
const utility = {}
const path = require('path')
const fs = require('fs')
const Blog = require('../model/Blog')

utility.uploadProfilePhoto = () => {
    const storage = multer.diskStorage({
        destination: path.join(__dirname, '../public/profile_photos'),
        filename: (req, file, cb) => {
            if (!(file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg')) {
                return cb('file type must be in .png, .jpg, .jpeg')
            }
            return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`)
        }
    })

    const upload = multer({
        storage: storage,
        limits: 1024 * 1024 * 5
    })
    return upload
}

utility.uploadCoverPhoto = () => {
    const storage = multer.diskStorage({
        destination: path.join(__dirname, '../public/cover_photos'),
        filename: (req, file, cb) => {
            if (!(file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg')) {
                return cb('file type must be in .png, .jpg, .jpeg')
            }
            return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`)
        }
    })

    const upload = multer({
        storage: storage,
        limits: 1024 * 1024 * 5
    })
    return upload
}

utility.removeExtraPhotos = (filename, folder) => {
    const photos = fs.readdirSync(path.join(__dirname, `../public/${folder}`))
    photos.map(e => {
        if (e === filename) fs.unlinkSync(path.join(__dirname, `../public/${folder}/${filename}`))
    })
}

utility.eventEmitter = (time, title) => {
    const events = require('events')
    const emitter = new events.EventEmitter()

    emitter.once('makeTrue', () => {
        setTimeout(async () => {
            await Blog.updateOne({ title: title }, {$set: {isPublished: true}})
            console.log("Blog published")
        }, time)
    })

    emitter.emit('makeTrue')
}

utility.getTime = (date) => {
    if(!date) return undefined
    const now = new Date(date).toISOString()
    return now
}


module.exports = utility

