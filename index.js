const express = require('express')
const app = express()
require('dotenv').config()
const port = process.env.PORT || 3000
const routes = require('./routes/routes')
const bodyParser = require('body-parser')

app.use(bodyParser.json())
app.use('/', routes)

app.listen(port, () => {
    console.log(`Successfully listening on ${port}`)
})