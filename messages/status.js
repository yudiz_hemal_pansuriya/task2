const status = {
    createdStatus: 201, // register, postblog, createuser
    successStatus: 200,
    nocontentStatus: 204, // delete
    badRequestStatus: 400,// invalid
    unauthorizedStatus: 401, // headermissing 
    forbiddenStatus: 403, // admin can
    notFoundStatus: 404,
    internalServerErrorStatus: 500
}

module.exports = status