const messages = {
    passwordValidateMsg : { message: 'password should must contain at least one number and special character and length must between 5 to 17' },
    userCreatedMsg :  'User created' ,
    profileDelSuccessMsg: { message: 'Profile deleted successFully' },
    registeredSuccessMsg: { message: 'Registered Successfully!' },
    alreadyLoginMsg : 'You are already logged in',
    loginSuccessMsg : 'Login Successfully!',
    incorrectPasswordMsg: { message: 'Incorrect Password' },
    userNotFound: { message: 'User Not Found!' },
    dataUpdateSuccessMsg: { message: "Data updated Successfully" },
    passwordChangedSuccessMsg: { message: "password changed successfully" },
    loggedOutSuccessMsg: { message: "Logged out successfully" },
    invalidIdMsg: { message: 'Invalid id' },
    passwordResetSuccessMsg: { message: 'Password reset successfully' },
    invalidDateMsg: { message: 'value is not valid, Enter date from future' },
    blogPostedSuccessMsg: { message: "Blog is posted" },
    blogNotPublishedMsg: { message: 'The blog is not published' },
    unlikeSuccessMsg: { message: 'Unliked successfully' },
    likeSuccessMsg: { message: 'liked successfully' },
    routeNotFound: { message: 'Route not found' },
    needtToLoginFirstMsg: { message: 'You need to login first' },
    needToLoginAgainMsg: 'need to login again',
    headerMissingMsg: { message: 'Authorization header is missing!' },
    onlyAdminCanMsg: { message: 'Only admin can access this route' },
    notEditPasswordMsg: { message: 'You can not edit password here' },
    searchUsingMsg: { message: 'You can only search using id, email or userName ' }
}

module.exports = messages